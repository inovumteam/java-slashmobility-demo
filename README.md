## Project configuration
1. Configure your database credentials in application.properties file
2. Use `mvn clean install` to install all packages.
3. Use `mvn spring-boot:run` to run the app .
4. Go to http://localhost:8080/swagger-ui/index.html?configUrl=/v3/api-docs/swagger-config to use Swagger UI and test the API.

## Tests
1. `mvn test` to run tests
2. On March 2020 the app is deployed for one month on https://slashmobility-spring.herokuapp.com/swagger-ui/index.html?configUrl=/v3/api-docs/swagger-config


