package com.inovum.api.mappers;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;


import com.inovum.api.models.dto.ProductDTO;
import com.inovum.api.models.entities.Product;
import com.inovum.api.models.entities.Provider;


@Mapper
public interface ProductMapper {
	public static ProductMapper INSTANCE = Mappers.getMapper(ProductMapper.class);

	ProductDTO entityToDto(Product entity);

	Product dtoToEntity(ProductDTO dto);

	default String mapProvider(Provider provider) {
		return provider.getName();
	}

	default Provider mapProvider(String provider) {
		Provider prov = new Provider();
		prov.setName(provider);
		return prov;
	}

}
