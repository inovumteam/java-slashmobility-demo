package com.inovum.api.mappers;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import com.inovum.api.models.dto.ProviderDTO;
import com.inovum.api.models.entities.Provider;

@Mapper
public interface ProviderMapper {
	ProviderMapper INSTANCE = Mappers.getMapper(ProviderMapper.class);

	ProviderDTO entityToDto(Provider entity);

	Provider dtoToEntity(ProviderDTO dto);
}
