package com.inovum.api.mappers;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import com.inovum.api.models.dto.UserDTO;
import com.inovum.api.models.entities.User;

@Mapper
public interface UserMapper {
	UserMapper INSTANCE = Mappers.getMapper(UserMapper.class);
	UserDTO entityToDto(User entity); 

	User dtoToEntity(UserDTO dto);
}
