package com.inovum.api;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.enums.SecuritySchemeType;
import io.swagger.v3.oas.annotations.info.Contact;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.security.SecurityScheme;
import io.swagger.v3.oas.annotations.servers.Server;

@OpenAPIDefinition(
	info = @Info(
			title = "Slashmobility & Inovum Cooperation API",
			version = "v2",
			description = "This app provides REST APIs for products",
			contact = @Contact(
					name = "Inovum Solutions",
					email = "info@inovum-solutions.com",
					url = "https://inovum-solutions.com"
			)
	),
	servers = {
			@Server( 
					url="http://localhost:8080/",
					description="DEV Server"
			),
			@Server( 
					url="https://slashmobility-spring.herokuapp.com/",
					description="PROD Server"
			)
	}
)
@SecurityScheme(name = "JwtToken", type = SecuritySchemeType.HTTP, scheme = "bearer", bearerFormat = "jwt")
public class OpenApiConfig {

}
