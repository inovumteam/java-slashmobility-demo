package com.inovum.api.models.dto;

import lombok.Getter;
import lombok.Setter;
@Getter
@Setter
public class PasswordResetDTO {
	String token;
	String password;
}

