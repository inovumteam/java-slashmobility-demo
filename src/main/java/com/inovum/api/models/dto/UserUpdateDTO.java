package com.inovum.api.models.dto;

import com.sun.istack.NotNull;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserUpdateDTO {
	String name;
	String lastname;
	@NotNull
	@Schema(example = "test@example.com")
	String email;
}
