package com.inovum.api.models.dto;

import javax.persistence.Id;

import com.sun.istack.NotNull;

import io.swagger.v3.oas.annotations.Hidden;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserDTO {
	@Id
	@NotNull
	@Hidden
	Long id;
	@NotNull
	@Schema(example = "testuser")
	String username;
	String name;
	String lastname;
	@NotNull
	@Schema(example = "test@example.com")
	String email;
	@NotNull
	@Schema(example = "12345")
	String password;
}
