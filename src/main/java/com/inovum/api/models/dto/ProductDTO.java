package com.inovum.api.models.dto;

import io.swagger.v3.oas.annotations.Hidden;
import io.swagger.v3.oas.annotations.media.Schema;
import javax.persistence.Id;

import com.sun.istack.NotNull;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Schema
public class ProductDTO {
	@Id
	@NotNull
	@Hidden
	Long id;
	@NotNull
	@Schema(example = "s-shirt")
	String name;
	@NotNull
	@Schema(example = "clothes")
	String type;
	@NotNull
	@Schema(example = "t-shirt.jpg")
	String image;
	@NotNull
	@Schema(example = "lorem ipsum...")
	String description;
	@NotNull
	@Schema(example = "clothing provider")
	String provider;
}
