package com.inovum.api.models.dto;

import javax.persistence.Id;

import com.sun.istack.NotNull;

import io.swagger.v3.oas.annotations.Hidden;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProviderDTO {
	
	@Id
	@Hidden
	Long id;
	@NotNull
	@Schema(example = "clothing provider")
	String name;
	@NotNull
	@Schema(example = "Plaza Cataluña")
	String address;
	@NotNull
	@Schema(example = "+34 638 422 345")
	String telephone;
	@NotNull
	@Schema(example = "Barcelona")
	String city;
}
