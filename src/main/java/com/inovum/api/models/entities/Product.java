package com.inovum.api.models.entities;

import java.io.Serializable;
import javax.persistence.*;

import javax.validation.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "products")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Product implements Serializable {
    private static final long serialVersionUID = 1L;
	@JsonProperty(access = JsonProperty.Access.READ_ONLY)
	private @Id @GeneratedValue Long id;
	@NotBlank(message = "Name is mandatory")
	private String name;
	private String type;
	private String description;
	private String image;

	@ManyToOne
	@JoinColumn(name = "provider_name", referencedColumnName = "name")
	private Provider provider;

}
