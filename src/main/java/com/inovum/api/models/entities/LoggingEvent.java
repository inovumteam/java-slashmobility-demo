package com.inovum.api.models.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "logging_event")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class LoggingEvent implements Serializable {

	private static final long serialVersionUID = 1L;
	private @Id @GeneratedValue Long event_id;
	private String formatted_message;
	private String level_string;
}
