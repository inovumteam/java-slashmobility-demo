package com.inovum.api.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.inovum.api.models.dto.ProductDTO;
import com.inovum.api.models.entities.Product;
import com.inovum.api.models.entities.Provider;
import com.inovum.api.mappers.ProductMapper;
import com.inovum.api.repositories.ProductRepository;
import com.inovum.api.repositories.ProviderRepository;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class ProductService {

	@Autowired
	private ProductRepository productsRepository;
	@Autowired
	private ProviderRepository providersRepository;

	/**
	 * Gets product by id
	 * 
	 * @param id
	 * @return
	 * @throws Exception
	 */
	@Transactional
	public Product getProduct(Long id) throws Exception {
		Product product = this.productsRepository.findById(id)
				.orElseThrow(() -> new Exception("Product with id " + id + " not found"));
		return product;
	}

	/**
	 * Gets products by type
	 * 
	 * @param type
	 * @return
	 * @throws Exception
	 */
	@Transactional
	public List<Product> getProductsByType(String type) throws Exception {
		List<Product> products = this.productsRepository.findAllByType(type)
				.orElseThrow(() -> new Exception("Products with type " + type + " not found"));
		return products;
	}

	/**
	 * Gets products by city
	 * 
	 * @param city
	 * @return
	 * @throws Exception
	 */
	@Transactional
	public List<Product> getProductsByCity(String city) throws Exception {
		List<Provider> providers = this.providersRepository.findAllByCity(city)
				.orElseThrow(() -> new Exception("Products with city " + city + " not found"));
		List<Product> result = new ArrayList<>();
		for (Provider provider : providers) {
			Optional<List<Product>> productsResult = this.productsRepository.findAllByProvider(provider);
			if (productsResult.isPresent()) {
				List<Product> products = productsResult.get();
				result.addAll(products);;
			}
		}
		if (result.isEmpty()) {
			new Exception("Products with city " + city + " not found");
		}
		return result;
	}

	/**
	 * Gets product list
	 * 
	 * @return
	 * @throws Exception
	 */
	@Transactional
	public List<Product> getProducts() throws Exception {
		List<Product> productsList = productsRepository.findAll();
		if (productsList.isEmpty()) {
			new Exception("No products found");
		}
		return productsList;
	}

	/**
	 * Creates and saves new product
	 * 
	 * @param product
	 * @return
	 * @throws Exception
	 */
	@Transactional
	public Product createProduct(ProductDTO product) throws Exception {
		Provider provider = this.providersRepository.findByName(product.getProvider())
				.orElseThrow(() -> new Exception("Provider with name " + product.getProvider() + " not found"));

		Product productEntity = ProductMapper.INSTANCE.dtoToEntity(product);

		productEntity.setProvider(provider);
		return this.productsRepository.save(productEntity);
	}

	/**
	 * Updates product with new values
	 * 
	 * @param id
	 * @param product
	 * @return
	 * @throws Exception
	 */
	@Transactional
	public Product updateProduct(Long id, ProductDTO product) throws Exception {
		this.productsRepository.findById(id).orElseThrow(() -> new Exception("Product with id " + id + " not found"));
		Provider provider = this.providersRepository.findByName(product.getProvider())
				.orElseThrow(() -> new Exception("Provider with name " + product.getProvider() + " not found"));
		Product productEntity = ProductMapper.INSTANCE.dtoToEntity(product);
		productEntity.setId(id);
		productEntity.setProvider(provider);
		return this.productsRepository.save(productEntity);
	}

}
