package com.inovum.api.services;

import java.util.Calendar;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.inovum.api.models.dto.UserDTO;
import com.inovum.api.models.dto.UserUpdateDTO;
import com.inovum.api.models.entities.User;
import com.inovum.api.mappers.UserMapper;
import com.inovum.api.models.entities.ConfirmationToken;
import com.inovum.api.models.entities.PasswordResetToken;
import com.inovum.api.repositories.ConfirmationTokenRepository;
import com.inovum.api.repositories.PasswordTokenRepository;
import com.inovum.api.repositories.UserRepository;

@Service
public class UserService {
	@Autowired
	private UserRepository usersRepository;
	@Autowired
	private ConfirmationTokenRepository confirmationTokenRepository;
	@Autowired
	private PasswordTokenRepository passwordTokenRepository;

	/**
	 * Gets user by id
	 * 
	 * @param id
	 * @return
	 * @throws Exception
	 */
	@Transactional
	public User getUser(Long id) throws Exception {
		User user = this.usersRepository.findById(id)
				.orElseThrow(() -> new Exception("User with id " + id + " not found"));
		return user;
	}

	/**
	 * Gets user list
	 * 
	 * @return
	 * @throws Exception
	 */
	@Transactional
	public List<User> getUsers() throws Exception {
		List<User> usersList = usersRepository.findAll();
		if (usersList.isEmpty()) {
			new Exception("No users found");
		}
		return usersList;
	}

	/**
	 * Creates new user
	 * 
	 * @param user
	 * @return
	 * @throws Exception
	 */
	@Transactional
	public User createUser(UserDTO user) throws Exception {
		if (this.usersRepository.findByEmailIgnoreCase(user.getEmail()).isPresent()) {
			throw new Exception("email already exists");
		}
		if (this.usersRepository.findByUsername(user.getUsername()).isPresent()) {
			throw new Exception("username already exists");
		}
		// encrypting password
		BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
		String hashedPassword = passwordEncoder.encode(user.getPassword());
		user.setPassword(hashedPassword);
		User userEntity = UserMapper.INSTANCE.dtoToEntity(user);
		this.usersRepository.save(userEntity);
		// creating confirmation token
		ConfirmationToken confirmationToken = new ConfirmationToken(userEntity);
		confirmationTokenRepository.save(confirmationToken);

		return userEntity;
	}

	/**
	 * Updates user with new values
	 * 
	 * @param id
	 * @param user
	 * @return
	 * @throws Exception
	 */
	@Transactional
	public User updateUser(Long id, UserUpdateDTO user) throws Exception {
		User oldUser = this.usersRepository.findById(id)
				.orElseThrow(() -> new Exception("User with id " + id + " not found"));
		if (!oldUser.getEmail().equalsIgnoreCase(user.getEmail())
				&& this.usersRepository.findByEmailIgnoreCase(user.getEmail()).isPresent()) {
			throw new Exception("email already exists");
		}
		oldUser.setId(id);
		oldUser.setName(user.getName());
		oldUser.setLastname(user.getLastname());
		oldUser.setEmail(user.getEmail());
		return this.usersRepository.save(oldUser);
	}

	/**
	 * Updates logged user with new values
	 * 
	 * @param id
	 * @param user
	 * @return
	 * @return
	 * @throws Exception
	 */
	@Transactional
	public User updateLoggedUser(UserUpdateDTO user) throws Exception {
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		String username = ((UserDetails) principal).getUsername();
		User userEntity = this.usersRepository.findByUsername(username).get();
		return this.updateUser(userEntity.getId(), user);
	}

	/**
	 * Creates password reset token
	 * 
	 * @param user
	 * @param token
	 * @return
	 */
	@Transactional
	public PasswordResetToken createPasswordResetTokenForUser() {
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		String username = ((UserDetails) principal).getUsername();
		User user = this.usersRepository.findByUsername(username).get();
		String token = UUID.randomUUID().toString();
		PasswordResetToken myToken = new PasswordResetToken(token, user, false);
		return passwordTokenRepository.save(myToken);
	}

	/**
	 * Resets user password
	 * 
	 * @param token
	 * @param password
	 * @return
	 * @throws Exception
	 */
	@Transactional
	public boolean resetPassword(String token, String password) throws Exception {
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		String username = ((UserDetails) principal).getUsername();

		PasswordResetToken passwordToken = passwordTokenRepository.findByToken(token);
		if ((passwordToken != null) && (passwordToken.getUsed() == false)
				&& (passwordToken.getUser().getUsername().equalsIgnoreCase(username))) {

			Calendar cal = Calendar.getInstance();
			if ((passwordToken.getExpiryDate().getTime() - cal.getTime().getTime()) <= 0) {
				throw new Exception("Expired token");
			}
			User user = this.usersRepository.findByUsername(username).get();
			// encrypting password
			BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
			String hashedPassword = passwordEncoder.encode(password);
			user.setPassword(hashedPassword);
			this.usersRepository.save(user);
			passwordToken.setUsed(true);
			passwordTokenRepository.save(passwordToken);
			return true;
		} else {
			throw new Exception("Invalid token");
		}
	}
}
