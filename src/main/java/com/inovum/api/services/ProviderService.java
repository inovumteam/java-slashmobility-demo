package com.inovum.api.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.inovum.api.models.dto.ProviderDTO;
import com.inovum.api.models.entities.Provider;
import com.inovum.api.mappers.ProviderMapper;
import com.inovum.api.repositories.ProviderRepository;

@Service
public class ProviderService {

	@Autowired
	private ProviderRepository providersRepository;

	/**
	 * Gets provider by id
	 * 
	 * @param name
	 * @return
	 * @throws Exception
	 */
	@Transactional
	public Provider getProvider(Long id) throws Exception {
		Provider provider = this.providersRepository.findById(id)
				.orElseThrow(() -> new Exception("Provider with id " + id + " not found"));
		return provider;
	}

	/**
	 * Gets provider list
	 * 
	 * @return
	 * @throws Exception
	 */
	@Transactional
	public List<Provider> getProviders() throws Exception {
		List<Provider> providersList = providersRepository.findAll();
		if (providersList.isEmpty()) {
			new Exception("No providers found");
		}
		return providersList;
	}

	/**
	 * Creates and saves new provider
	 * 
	 * @param provider
	 * @return
	 * @throws Exception
	 */
	@Transactional
	public Provider createProvider(ProviderDTO provider) throws Exception {
		if (this.providersRepository.findByName(provider.getName()).isPresent()) {
			throw new Exception("provider with this name already exists");
		}
		Provider providerEntity = ProviderMapper.INSTANCE.dtoToEntity(provider);
		return this.providersRepository.save(providerEntity);
	}

	/**
	 * Updates provider with new values
	 * 
	 * @param name
	 * @param provider
	 * @return
	 * @throws Exception
	 */
	@Transactional
	public Provider updateProvider(Long id, ProviderDTO provider) throws Exception {
		this.providersRepository.findById(id).orElseThrow(() -> new Exception("Provider with id " + id + " not found"));
		Optional<Provider> checkProvider = this.providersRepository.findByName(provider.getName());
		if (checkProvider.isPresent()) {
			Provider prov = checkProvider.get();
			if (prov.getId() != id) {
				throw new Exception("provider with this name already exists");
			}
		}
		Provider providerEntity = ProviderMapper.INSTANCE.dtoToEntity(provider);
		providerEntity.setId(id);
		return this.providersRepository.save(providerEntity);
	}

}
