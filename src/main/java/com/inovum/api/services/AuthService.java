package com.inovum.api.services;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.inovum.api.controllers.AuthController;
import com.inovum.api.models.dto.AuthenticationRequestDTO;
import com.inovum.api.models.entities.JwtBlacklist;
import com.inovum.api.repositories.JwtBlacklistRepository;
import com.inovum.api.repositories.UserRepository;
import com.inovum.api.security.jwt.JwtTokenProvider;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class AuthService {

	@Autowired
	private JwtTokenProvider jwtTokenProvider;

	@Autowired
	private UserRepository usersRepository;
	@Autowired
	public JwtBlacklistRepository jwtBlacklistRepository;
	@Autowired
	private AuthenticationManager authenticationManager;

	/**
	 * creates JWT token for user
	 * 
	 * @param username
	 * @return
	 */
	public String createJwtToken(String username) {
		String token = jwtTokenProvider.createToken(username, this.usersRepository.findByUsername(username)
				.orElseThrow(() -> new UsernameNotFoundException("Username " + username + "not found")).getRoles());
		return token;
	}

	/**
	 * Authenticates user with credentials
	 * 
	 * @param user
	 * @return
	 */
	public Map<String, String> authenticate(AuthenticationRequestDTO user) {
		String username = user.getUsername();
		String password = user.getPassword();
		authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
		String token = this.createJwtToken(username);
		Map<String, String> model = new HashMap<String, String>();
		model.put("username", username);
		model.put("token", token);
		log.info("User " + user.getUsername() + " logged into system");
		return model;
	}

	/**
	 * Adds token to blacklist in order to logout user
	 * 
	 * @param token
	 * @return
	 */
	public JwtBlacklist addTokenToBlacklist(String token) {
		JwtBlacklist jwtBlacklist = new JwtBlacklist();
		jwtBlacklist.setToken(token);
		return this.jwtBlacklistRepository.save(jwtBlacklist);
	}

	/*
	 * Returns logged user username
	 */
	public String getLoggedUserUsername() {
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		String username = ((UserDetails) principal).getUsername();
		return username;
	}
}
