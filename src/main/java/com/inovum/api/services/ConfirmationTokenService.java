package com.inovum.api.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.inovum.api.models.entities.ConfirmationToken;
import com.inovum.api.models.entities.User;
import com.inovum.api.repositories.ConfirmationTokenRepository;
import com.inovum.api.repositories.UserRepository;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class ConfirmationTokenService {
	@Autowired
	private ConfirmationTokenRepository confirmationTokenRepository;
	@Autowired
	private UserRepository usersRepository;

	/**
	 * Creates confirmation token for user
	 * 
	 * @param user
	 * @return
	 * @throws Exception
	 */
	public String createToken(User user) throws Exception {
		ConfirmationToken confirmationToken = new ConfirmationToken(user);
		confirmationTokenRepository.save(confirmationToken);
		return confirmationToken.getConfirmationToken();
	}

	/**
	 * Validates user using confirmation token
	 * 
	 * @param confirmationToken
	 * @throws Exception
	 */
	public void acceptConfirmation(String confirmationToken) throws Exception {
		ConfirmationToken token = confirmationTokenRepository.findByConfirmationToken(confirmationToken);
		if (token != null) {
			Optional<User> user = this.usersRepository.findByEmailIgnoreCase(token.getUser().getEmail());
			User userEntity = user.get();
			userEntity.setEnabled(true);
			usersRepository.save(userEntity);
			confirmationTokenRepository.delete(token);
			log.info("User " + userEntity.getUsername() + " account successfully verified");
		} else {
			throw new Exception("Invalid or expired confirmation token");
		}
	}
}