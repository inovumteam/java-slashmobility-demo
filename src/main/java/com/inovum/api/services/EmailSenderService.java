package com.inovum.api.services;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

@Service
public class EmailSenderService {
	@Autowired
	private JavaMailSender javaMailSender;

	/**
	 * Sends email
	 * 
	 * @param email
	 */
	@Async
	public void sendEmail(SimpleMailMessage email) {
		javaMailSender.send(email);
	}

	/**
	 * Sends account confirmation link
	 * 
	 * @param receiverEmail
	 * @param confirmationToken
	 */
	public void sendConfirmationEmail(String receiverEmail, String confirmationToken) {
		SimpleMailMessage mailMessage = new SimpleMailMessage();
		mailMessage.setTo(receiverEmail);
		mailMessage.setSubject("Complete your registration");
		mailMessage.setFrom("gianluca@inovum-solutions.com");
		mailMessage.setText("To confirm your account, please click here: " + getBaseEnvLinkURL()
				+ "/auth/confirm-account?token=" + confirmationToken);
		this.sendEmail(mailMessage);
	}

	/**
	 * Sends password reset request token
	 * 
	 * @param receiverEmail
	 * @param token
	 */
	public void sendPasswordResetEmail(String receiverEmail, String token) {
		SimpleMailMessage mailMessage = new SimpleMailMessage();
		mailMessage.setTo(receiverEmail);
		mailMessage.setSubject("Password reset request");
		mailMessage.setFrom("gianluca@inovum-solutions.com");
		mailMessage.setText("Use this reset token to reset your password: " + token);
		this.sendEmail(mailMessage);
	}

	/**
	 * Gets base url
	 * 
	 * @return
	 */
	protected static String getBaseEnvLinkURL() {

		String baseEnvLinkURL = null;
		HttpServletRequest currentRequest = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes())
				.getRequest();
		baseEnvLinkURL = "http://" + currentRequest.getLocalName();
		if (currentRequest.getLocalPort() != 80) {
			baseEnvLinkURL += ":" + currentRequest.getLocalPort();
		}
		if (!StringUtils.isEmpty(currentRequest.getContextPath())) {
			baseEnvLinkURL += currentRequest.getContextPath();
		}
		return baseEnvLinkURL;
	}

}