package com.inovum.api.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.inovum.api.models.entities.LoggingEvent;

public interface LoggingEventRepository extends JpaRepository<LoggingEvent, Long> {

}