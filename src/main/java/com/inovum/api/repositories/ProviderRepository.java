package com.inovum.api.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.inovum.api.models.entities.Provider;

public interface ProviderRepository extends JpaRepository<Provider, Long> {
	Optional<Provider> findByName(String name);

	Optional<List<Provider>> findAllByCity(String city);
}