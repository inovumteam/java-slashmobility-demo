package com.inovum.api.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.inovum.api.models.entities.Product;
import com.inovum.api.models.entities.Provider;

public interface ProductRepository extends JpaRepository<Product, Long> {
	Optional<Product> findById(Long id);

	Optional<List<Product>> findAllByType(String type);

	Optional<List<Product>> findAllByProvider(Provider provider);
}