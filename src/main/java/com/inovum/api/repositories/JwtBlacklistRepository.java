package com.inovum.api.repositories;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.inovum.api.models.entities.JwtBlacklist;

public interface JwtBlacklistRepository extends JpaRepository<JwtBlacklist, Long> {
	Optional<JwtBlacklist> findByToken(String token);
}
