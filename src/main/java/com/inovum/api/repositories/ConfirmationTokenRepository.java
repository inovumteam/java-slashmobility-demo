package com.inovum.api.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.inovum.api.models.entities.ConfirmationToken;

public interface ConfirmationTokenRepository extends JpaRepository<ConfirmationToken, String> {
	ConfirmationToken findByConfirmationToken(String confirmationToken);
}