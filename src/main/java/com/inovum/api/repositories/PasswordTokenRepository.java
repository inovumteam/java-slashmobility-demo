package com.inovum.api.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.inovum.api.models.entities.PasswordResetToken;

public interface PasswordTokenRepository extends JpaRepository<PasswordResetToken, Long> {
	PasswordResetToken findByToken(String token);
}
