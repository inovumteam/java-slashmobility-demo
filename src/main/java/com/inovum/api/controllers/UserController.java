package com.inovum.api.controllers;

import java.util.List;
import org.springframework.web.bind.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.inovum.api.models.dto.UserUpdateDTO;
import com.inovum.api.models.entities.User;
import com.inovum.api.services.UserService;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/users")
public class UserController {
	@Autowired
	private UserService userService;

	/**
	 * Lists all existing users
	 * 
	 * @return
	 * @throws Exception
	 */
	@Operation(security = @SecurityRequirement(name = "JwtToken"))
	@GetMapping("")
	public ResponseEntity<List<User>> list() throws Exception {
		log.info("Listing all users");
		return new ResponseEntity<List<User>>(this.userService.getUsers(), HttpStatus.OK);
	}

	/**
	 * Loads user by id
	 * 
	 * @param id
	 * @return
	 * @throws Exception
	 */
	@Operation(security = @SecurityRequirement(name = "JwtToken"))
	@GetMapping("/{id}")
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<User> findById(@PathVariable("id") Long id) throws Exception {
		log.info("Listing user by id: " + id);
		return new ResponseEntity<User>(this.userService.getUser(id), HttpStatus.OK);

	}

	/**
	 * Updates user information
	 * 
	 * @param user
	 * @return
	 * @throws Exception
	 */
	@Operation(security = @SecurityRequirement(name = "JwtToken"))
	@PutMapping("")
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<User> update(@RequestBody UserUpdateDTO user) throws Exception {
		log.info("Updating user with email " + user.getEmail());
		return new ResponseEntity<User>(this.userService.updateLoggedUser(user), HttpStatus.OK);

	}

}
