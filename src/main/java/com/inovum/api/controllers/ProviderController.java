package com.inovum.api.controllers;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.inovum.api.models.dto.ProviderDTO;
import com.inovum.api.models.entities.Provider;
import com.inovum.api.services.ProviderService;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/providers")
public class ProviderController {

	@Autowired
	private ProviderService providerService;

	/**
	 * Lists all existing providers
	 * 
	 * @return
	 * @throws Exception
	 */
	@Operation(security = @SecurityRequirement(name = "JwtToken"))
	@GetMapping("")
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<List<Provider>> all() throws Exception {
		log.info("Listing all providers");
		return new ResponseEntity<List<Provider>>(this.providerService.getProviders(), HttpStatus.OK);
	}

	/**
	 * Loads provider by name
	 * 
	 * @param name
	 * @return
	 * @throws Exception
	 */
	@Operation(security = @SecurityRequirement(name = "JwtToken"))
	@GetMapping("/{id}")
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<Provider> findById(@PathVariable("id") Long id) throws Exception {
		log.info("Listing provider by id: " + id);
		return new ResponseEntity<Provider>(this.providerService.getProvider(id), HttpStatus.OK);
	}

	/**
	 * Creates new provider
	 * 
	 * @param provider
	 * @return
	 * @throws Exception
	 */
	@Operation(security = @SecurityRequirement(name = "JwtToken"))
	@PostMapping("")
	@ResponseStatus(HttpStatus.OK)
	@Valid
	public ResponseEntity<Provider> create(@Valid @RequestBody ProviderDTO provider) throws Exception {
		log.info("Creating new provider: " + provider.getName());
		return new ResponseEntity<Provider>(this.providerService.createProvider(provider), HttpStatus.OK);
	}

	/**
	 * Updates provider
	 * 
	 * @param name
	 * @param provider
	 * @return
	 * @throws Exception
	 */
	@Operation(security = @SecurityRequirement(name = "JwtToken"))
	@PutMapping("/{id}")
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<Provider> update(@PathVariable("id") Long id, @RequestBody ProviderDTO provider)
			throws Exception {
		log.info("Updating provider with id " + id);
		return new ResponseEntity<Provider>(this.providerService.updateProvider(id, provider), HttpStatus.OK);
	}

}
