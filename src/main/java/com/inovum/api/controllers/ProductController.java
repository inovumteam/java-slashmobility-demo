package com.inovum.api.controllers;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.inovum.api.models.dto.ProductDTO;
import com.inovum.api.models.entities.Product;
import com.inovum.api.services.ProductService;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/products")
public class ProductController {

	@Autowired
	private ProductService productService;

	/**
	 * Lists all existing products
	 * 
	 * @return
	 * @throws Exception
	 */
	@Operation(security = @SecurityRequirement(name = "JwtToken"))
	@GetMapping("")
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<List<Product>> all() throws Exception {
		log.info("Listing all products");
		return new ResponseEntity<List<Product>>(this.productService.getProducts(), HttpStatus.OK);
	}

	/**
	 * Loads product by id
	 * 
	 * @param id
	 * @return
	 * @throws Exception
	 */
	@Operation(security = @SecurityRequirement(name = "JwtToken"))
	@GetMapping(value = "/{id}")
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<Product> findById(@PathVariable("id") Long id) throws Exception {
		log.info("Listing product by id: " + id);
		return new ResponseEntity<Product>(this.productService.getProduct(id), HttpStatus.OK);
	}

	/**
	 * Loads product by type
	 * 
	 * @param type
	 * @return
	 * @throws Exception
	 */
	@Operation(security = @SecurityRequirement(name = "JwtToken"))
	@GetMapping(value = "/type/{type}")
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<List<Product>> findByType(@PathVariable("type") String type) throws Exception {
		log.info("Listing products by type: " + type);
		return new ResponseEntity<List<Product>>(this.productService.getProductsByType(type), HttpStatus.OK);
	}

	/**
	 * Loads product by providers city
	 * 
	 * @param city
	 * @return
	 * @throws Exception
	 */
	@Operation(security = @SecurityRequirement(name = "JwtToken"))
	@GetMapping(value = "/city/{city}")
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<List<Product>> findByCity(@PathVariable("city") String city) throws Exception {
		log.info("Listing products by city: " + city);
		List<Product> prods = this.productService.getProductsByCity(city);
		return new ResponseEntity<List<Product>>(prods, HttpStatus.OK);
	}

	/**
	 * Creates new product
	 * 
	 * @param product
	 * @return
	 * @throws Exception
	 */
	@Operation(security = @SecurityRequirement(name = "JwtToken"))
	@PostMapping("")
	@ResponseStatus(HttpStatus.OK)
	@Valid
	public ResponseEntity<Product> create(@Valid @RequestBody ProductDTO product) throws Exception {
		log.info("Creating new product: " + product.getName());
		return new ResponseEntity<Product>(this.productService.createProduct(product), HttpStatus.OK);
	}

	/**
	 * Updates product
	 * 
	 * @param id
	 * @param product
	 * @return
	 * @throws Exception
	 */
	@Operation(security = @SecurityRequirement(name = "JwtToken"))
	@PutMapping(value = "/{id}")
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<Product> update(@PathVariable("id") Long id, @RequestBody ProductDTO product)
			throws Exception {
		log.info("Updating product with id " + id);
		return new ResponseEntity<Product>(this.productService.updateProduct(id, product), HttpStatus.OK);
	}

}
