package com.inovum.api.controllers;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.inovum.api.models.dto.AuthenticationRequestDTO;
import com.inovum.api.models.dto.PasswordResetDTO;
import com.inovum.api.models.dto.UserDTO;
import com.inovum.api.models.entities.User;
import com.inovum.api.security.jwt.JwtTokenProvider;
import com.inovum.api.models.entities.PasswordResetToken;
import com.inovum.api.services.AuthService;
import com.inovum.api.services.ConfirmationTokenService;
import com.inovum.api.services.EmailSenderService;
import com.inovum.api.services.UserService;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/auth")
public class AuthController {

	@Autowired
	private UserService userService;

	@Autowired
	private EmailSenderService emailSenderService;

	@Autowired
	private ConfirmationTokenService confirmationTokenService;

	@Autowired
	private AuthService authService;

	@Autowired
	private JwtTokenProvider jwtTokenProvider;

	/**
	 * Authenticate user in system
	 * 
	 * @param data
	 * @return
	 */
	@PostMapping("/signin")
	public ResponseEntity<Map<String, String>> signin(@RequestBody AuthenticationRequestDTO data) {
		return new ResponseEntity<Map<String, String>>(this.authService.authenticate(data), HttpStatus.OK);
	}

	/**
	 * Registers new user
	 * 
	 * @param user
	 * @return
	 * @throws Exception
	 */
	@PostMapping("/signup")
	@Valid
	ResponseEntity<User> signup(@Valid @RequestBody UserDTO user) throws Exception {
		User createdUser = this.userService.createUser(user);
		String confirmationToken = this.confirmationTokenService.createToken(createdUser);
		this.emailSenderService.sendConfirmationEmail(createdUser.getEmail(), confirmationToken);
		log.info("New user " + createdUser.getUsername() + " registered");
		return new ResponseEntity<User>(createdUser, HttpStatus.OK);
	}

	/**
	 * Validates user account with confirmation token
	 * 
	 * @param confirmationToken
	 * @return
	 * @throws Exception
	 */
	@GetMapping("/confirm-account")
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<Void> confirmUserAccount(@RequestParam("token") String confirmationToken) throws Exception {
		this.confirmationTokenService.acceptConfirmation(confirmationToken);
		return new ResponseEntity<>(HttpStatus.OK);
	}

	/**
	 * Logs out user
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@Operation(security = @SecurityRequirement(name = "JwtToken"))
	@GetMapping("/logout")
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<Void> logout(HttpServletRequest request, HttpServletResponse response) {
		String token = jwtTokenProvider.resolveToken((HttpServletRequest) request);
		this.authService.addTokenToBlacklist(token);
		log.info("User " + authService.getLoggedUserUsername() + " logged out");
		return new ResponseEntity<>(HttpStatus.OK);
	}

	/**
	 * Creates password reset request
	 * 
	 * @param request
	 * @return
	 */
	@Operation(security = @SecurityRequirement(name = "JwtToken"))
	@GetMapping("/reset-password")
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<Void> resetPassword(HttpServletRequest request) {
		PasswordResetToken token = this.userService.createPasswordResetTokenForUser();
		this.emailSenderService.sendPasswordResetEmail(token.getUser().getEmail(), token.getToken());
		log.info("User " + authService.getLoggedUserUsername() + " requested for a password reset");
		return new ResponseEntity<>(HttpStatus.OK);
	}

	/**
	 * Checks password reset request and updates password for logged user
	 * 
	 * @param passwordReset
	 * @return
	 * @throws Exception
	 */
	@Operation(security = @SecurityRequirement(name = "JwtToken"))
	@PostMapping("/reset-password")
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<Void> changePassword(@RequestBody PasswordResetDTO passwordReset) throws Exception {
		if (this.userService.resetPassword(passwordReset.getToken(), passwordReset.getPassword())) {
			log.info("User " + authService.getLoggedUserUsername() + " performed a password reset");
			return new ResponseEntity<>(HttpStatus.OK);
		}
		return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
	}

}