package com.inovum.api.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.inovum.api.models.entities.LoggingEvent;

import com.inovum.api.repositories.LoggingEventRepository;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/logs")
public class LoggingEventController {

	@Autowired
	private LoggingEventRepository loggingEventRepository;

	/**
	 * Lists all existing logs
	 * 
	 * @return
	 * @throws Exception
	 */
	@Operation(security = @SecurityRequirement(name = "JwtToken"))
	@GetMapping("")
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<List<LoggingEvent>> all() throws Exception {
		log.info("Listing all logs");
		return new ResponseEntity<List<LoggingEvent>>(this.loggingEventRepository.findAll(), HttpStatus.OK);
	}

}
