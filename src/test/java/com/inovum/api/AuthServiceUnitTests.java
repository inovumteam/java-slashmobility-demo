package com.inovum.api;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;

import com.inovum.api.models.dto.UserDTO;
import com.inovum.api.models.entities.ConfirmationToken;
import com.inovum.api.models.entities.User;
import com.inovum.api.repositories.ConfirmationTokenRepository;
import com.inovum.api.repositories.UserRepository;
import com.inovum.api.services.UserService;

@ExtendWith(MockitoExtension.class)
@RunWith(JUnitPlatform.class)
public class AuthServiceUnitTests {
	@InjectMocks
	private UserService userService;

	@Mock
	private UserRepository userRepository;
	@Mock
	private ConfirmationTokenRepository confirmationTokenRepository;

	@Before
	public void setUp() throws Exception {
		userService = new UserService();
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testCreateUser() throws Exception {
		User userMock = new User();
		userMock.setUsername("testuser");
		userMock.setEmail("example@example.com");
		userMock.setName("Testuser");
		userMock.setLastname("Testuser");
		userMock.setPassword("123456");
		when(userRepository.save(Mockito.any(User.class))).thenReturn(userMock);
		when(userRepository.findByEmailIgnoreCase(Mockito.any(String.class))).thenReturn(Optional.<User>empty());
		when(userRepository.findByUsername(Mockito.any(String.class))).thenReturn(Optional.<User>empty());
		when(confirmationTokenRepository.save(Mockito.any(ConfirmationToken.class)))
				.thenReturn(new ConfirmationToken());
		UserDTO userToAdd = new UserDTO();
		userToAdd.setUsername("testuser");
		userToAdd.setEmail("example@example.com");
		userToAdd.setName("Testuser");
		userToAdd.setLastname("Testuser");
		userToAdd.setPassword("123456");
		User user = userService.createUser(userToAdd);
		assertThat(user.getName()).isEqualTo(userToAdd.getName());
		assertThat(user.getLastname()).isEqualTo(userToAdd.getLastname());
		assertThat(user.getEmail()).isEqualTo(userToAdd.getEmail());
	}

	@Test
	public void testGetUsers() throws Exception {
		List<User> users = new ArrayList<User>();
		users.add(new User((long) 1, "test1", "testuser1", "test", null, null, null, null));
		users.add(new User((long) 2, "test2", "testuser2", "test", null, null, null, null));
		users.add(new User((long) 3, "test3", "testuser3", "test", null, null, null, null));
		when(userRepository.findAll()).thenReturn(users);
		List<User> usersReturned = userService.getUsers();
		assertThat(users).isEqualTo(usersReturned);
	}

	@Test
	public void testGetUser() throws Exception {

		long id = 1;
		User mockUser = new User((long) 1, "test1", "testuser1", "test", null, null, null, null);
		when(userRepository.findById(Mockito.any(Long.class))).thenReturn(Optional.of(mockUser));
		User userReturned = userService.getUser(id);
		assertThat(mockUser).isEqualTo(userReturned);
	}

}
